﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineTimeUtil
{
    public class NoConnectionException : Exception
    {

        public NoConnectionException() : base("Could not connect to time server.") { }

        public NoConnectionException(Exception innerException) : base("Could not connect to time server.", innerException) { }

        public NoConnectionException(string message) : base(message) { }

        public NoConnectionException(string message, Exception innerException) : base(message, innerException) { }

    }
}
