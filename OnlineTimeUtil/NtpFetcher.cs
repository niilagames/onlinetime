﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Security;

namespace OnlineTimeUtil
{
    internal static class NtpFetcher
    {

        public static DateTime FetchTime()
        {
            string ntpServer = OnlineTimeSettings.NtpServerUrl;
            return AttemptFetchTime(ntpServer);
        }

        private static DateTime AttemptFetchTime(string ntpServer)
        {
            const int DaysTo1900 = 1900 * 365 + 95; // 95 = offset for leap-years etc.
            const long TicksPerSecond = 10000000L;
            const long TicksPerDay = 24 * 60 * 60 * TicksPerSecond;
            const long TicksTo1900 = DaysTo1900 * TicksPerDay;

            var ntpData = new byte[48];
            ntpData[0] = 0x1B; // LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)

            try
            {
                var addresses = Dns.GetHostEntry(ntpServer).AddressList;
                var ipEndPoint = new IPEndPoint(addresses[0], 123);
                long pingDuration = Stopwatch.GetTimestamp(); // temp access (JIT-Compiler need some time at first call)

                using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
                {
                    socket.Connect(ipEndPoint);
                    socket.ReceiveTimeout = 5000;
                    socket.Send(ntpData);
                    pingDuration = Stopwatch.GetTimestamp(); // after Send-Method to reduce WinSocket API-Call time

                    socket.Receive(ntpData);
                    pingDuration = Stopwatch.GetTimestamp() - pingDuration;
                }

                long pingTicks = pingDuration * TicksPerSecond / Stopwatch.Frequency;

                // optional: display response-time
                // Console.WriteLine("{0:N2} ms", new TimeSpan(pingTicks).TotalMilliseconds);

                long intPart = (long)ntpData[40] << 24 | (long)ntpData[41] << 16 | (long)ntpData[42] << 8 | ntpData[43];
                long fractPart = (long)ntpData[44] << 24 | (long)ntpData[45] << 16 | (long)ntpData[46] << 8 | ntpData[47];
                long netTicks = intPart * TicksPerSecond + (fractPart * TicksPerSecond >> 32);

                return new DateTime(TicksTo1900 + netTicks + pingTicks / 2);
            }
            // Catch exceptions that might be a result of disconnects and return a no connection exception, with the caught exception as inner exception.
            catch (Exception e) when (e is SocketException || e is SecurityException || e is InvalidOperationException || e is ArgumentOutOfRangeException)
            {
                throw new NoConnectionException(e);
            }
        }

    }
}
