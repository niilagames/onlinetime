﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace OnlineTimeUtil
{
    internal static class HttpFetcher
    {

        private static readonly HttpClient Client = new HttpClient();

        public static DateTime FetchTime()
        {
            // Validate settings.
            if (!IsValid(out string validationMessage))
            {
                throw new OnlineTimeSettingsException($"Settings not set properly for the HTTP fetch method:\n\n{validationMessage}");
            }

            // Get HTTP settings.
            string endpoint = OnlineTimeSettings.HttpEndpoint;
            HttpMethod httpMethod = OnlineTimeSettings.HttpMethod;
            bool supportsFormatRequest = OnlineTimeSettings.HttpSupportsFormatRequest;
            string formatPropertyName = OnlineTimeSettings.HttpFormatPropertyName;
            string dateTimeFormat = OnlineTimeSettings.HttpDateTimeFormat;

            try
            {
                HttpResponseMessage response = null;
                if (httpMethod == HttpMethod.Get)
                {
                    UriBuilder uriBuilder = new UriBuilder(endpoint)
                    {
                        Query = supportsFormatRequest ? $"{formatPropertyName}={dateTimeFormat}" : null,
                    };
                    Console.WriteLine($"URL: {uriBuilder.ToString()}");
                    response = Client.GetAsync(uriBuilder.ToString()).Result;
                }
                else
                {
                    string jsonString = $"{{{formatPropertyName}:{dateTimeFormat}}}";
                    response = Client.PostAsync(endpoint, new StringContent(jsonString)).Result;
                }

                if (!response.IsSuccessStatusCode)
                {
                    throw new NoConnectionException(
                        $"HTTP connection failed with status code {response.StatusCode} and message {response.ReasonPhrase}.");
                }

                // Parse the response to a date time object and return.
                return ParseResponse(response);
            }
            // When URL format is wrong.
            catch (UriFormatException e)
            {
                throw new ArgumentException(
                    $"The given HTTP endpoint is wrongly formatted ({endpoint}). See inner exception for details.", e);
            }
            // If an aggregate exception is caught.
            catch (AggregateException ae)
            {
                // Flatten the aggregate exception.
                var aeFlattened = ae.Flatten();

                aeFlattened.Handle(e =>
                {
                    if (e is WebException we)
                    {
                        HandleWebException(we, endpoint);
                    }

                    if (e is HttpRequestException hre && hre.InnerException is WebException hreWe)
                    {
                        HandleWebException(hreWe, endpoint);
                    }

                    return false;
                });

                //// Check if there is a web exception, first directly and then through a HTTP request exception.
                //WebException webException = aeFlattened.InnerExceptions.FirstOrDefault(e => e is WebException) as WebException ??
                //                            aeFlattened.InnerExceptions.FirstOrDefault(e => e is HttpRequestException)?.InnerException as
                //                                WebException;

                //// If no web exception was found, throw the flattened aggregate exception.
                //if (webException == null)
                //{
                //    throw aeFlattened;
                //}

                //// If we have a web exception, throw new NoConnectionException.
                //HandleWebException(webException, endpoint);
            }
            // When a HTTP request exception has been thrown with an inner web exception.
            catch (HttpRequestException e) when (e.InnerException is WebException)
            {
                // Get web exception
                WebException webException = (WebException) e.InnerException;
                HandleWebException(webException, endpoint);
            }
            // When a web exception has been thrown
            catch (WebException e)
            {
                HandleWebException(e, endpoint);
            }
            // If an HTTP request exception was thrown without a web exception as inner exception.
            catch (HttpRequestException e)
            {
                throw new NoConnectionException(
                    $"A HTTP request exception was thrown, while trying to reach HTTP endpoint. See inner exception for details.",
                    e);
            }

            throw new Exception("HTTP Fetcher exited the try catch block without returning, which should be impossible.");
        }

        private static void HandleWebException(WebException e, string endpoint)
        {
            switch (e.Status)
            {
                case WebExceptionStatus.NameResolutionFailure:
                    throw new NoConnectionException(
                        $"Couldn't resolve the HTTP endpoint (NameResolutionFailure). The given URL was ({endpoint}).",
                        e);
                case WebExceptionStatus.Timeout:
                    throw new NoConnectionException(
                        $"Timed out, while trying to reach the HTTP endpoint ({endpoint}).", e);
            }

            throw new NoConnectionException(
                $"A web exception was thrown, while trying to reach HTTP endpoint. See inner exception for details.",
                e);
        }

        private static bool IsValid(out string validationMessage)
        {
            List<string> validationMessageList = new List<string>();

            string endpoint = OnlineTimeSettings.HttpEndpoint;
            bool supportsFormatRequest = OnlineTimeSettings.HttpSupportsFormatRequest;
            string formatPropertyName = OnlineTimeSettings.HttpFormatPropertyName;
            string dateTimeFormat = OnlineTimeSettings.HttpDateTimeFormat;
            bool isReturningJson = OnlineTimeSettings.HttpReturnsJson;
            string jsonPath = OnlineTimeSettings.HttpJsonPath;

            if (string.IsNullOrWhiteSpace(endpoint))
            {
                validationMessageList.Add("An endpoint is required when using HTTP to fetch time.");
            }

            if (supportsFormatRequest && string.IsNullOrWhiteSpace(formatPropertyName))
            {
                validationMessageList.Add("When making use of format requesting, the format property name must be set.");
            }

            if (string.IsNullOrWhiteSpace(dateTimeFormat))
            {
                validationMessageList.Add("The date time format string must be set to what is expected to be received (and requested, if format request enabled).");
            }

            if (isReturningJson && string.IsNullOrWhiteSpace(jsonPath))
            {
                validationMessageList.Add("If a JSON object is returned from the HTTP call, a json path must be provided (separated by '.').");
            }

            // If we had validation errors.
            if (validationMessageList.Count > 0)
            {
                validationMessage = string.Join("\n", validationMessageList);
                return false;
            }

            // If no errors.
            validationMessage = null;
            return true;
        }

        private static DateTime ParseResponse(HttpResponseMessage response)
        {
            string dateTimeFormat = OnlineTimeSettings.HttpDateTimeFormat;
            bool isReturningJson = OnlineTimeSettings.HttpReturnsJson;
            string jsonPath = OnlineTimeSettings.HttpJsonPath;

            string dateTimeString = null;
            string responseString = response.Content.ReadAsStringAsync().Result;

            // If response is not JSON, expect a string.
            if (!isReturningJson)
            {
                dateTimeString = responseString;
            }
            // If it is a JSON response, parse to json and extract date string.
            else
            {
                JObject jsonObj = JObject.Parse(responseString); // Parse json.
                JToken dateToken = jsonObj.SelectToken(jsonPath); // Select token with path.
                dateTimeString = dateToken.Value<string>(); // Get value from token.
            }

            // Parse the date time string to a date time object.
            Console.WriteLine($"dateTimeString: {dateTimeString} - dateTimeFormat: {dateTimeFormat}");
            return DateTime.ParseExact(dateTimeString, dateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
        }
    }
}
