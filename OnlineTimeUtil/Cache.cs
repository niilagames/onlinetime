﻿using System;
using System.Threading.Tasks;
using TimeSpan = System.TimeSpan;

namespace OnlineTimeUtil
{
    internal static class Cache
    {

        private static CachedDateTime _cachedTime;

        /// <summary>
        /// Fetches time online, if no cache found, else returns the cached time.
        /// Can be forced to fetch online.
        /// </summary>
        /// <returns></returns>
        public static DateTime FetchTime(bool forceFetch = false)
        {
            if (_cachedTime == null || forceFetch)
            {
                var onlineTime = Fetcher.FetchTime();
                _cachedTime = new CachedDateTime(onlineTime);
            }

            return _cachedTime.CurrentTime;
        }

        /// <summary>
        /// Forces a fetch from online server to update the cache.
        /// </summary>
        public static void Sync()
        {
            var onlineTime = Fetcher.FetchTime();
            _cachedTime = new CachedDateTime(onlineTime);
        }

        /// <summary>
        /// Forces a fetch from online server to update the cache, but does so asynchronously.
        /// </summary>
        /// <returns>The task updating the cache.</returns>
        public static Task SyncAsync()
        {
            return Task.Run(() => {
                var onlineTime = Fetcher.FetchTime();
                _cachedTime = new CachedDateTime(onlineTime);
            });
        }

        /// <summary>
        /// Clears the cache.
        /// </summary>
        public static void Clear()
        {
            _cachedTime = null;
        }

        private class CachedDateTime
        {
            /// <summary>
            /// The online UTC time matching the cached local time <see cref="CachedLocalTime"/>.
            /// </summary>
            public DateTime CachedOnlineTime { get; private set; }
            /// <summary>
            /// The local UTC time matching the cached online time <see cref="CachedOnlineTime"/>.
            /// </summary>
            public DateTime CachedLocalTime { get; private set; }

            public DateTime CurrentTime
            {
                get
                {
                    // Get current local UTC time.
                    DateTime currentLocalTime = DateTime.UtcNow;
                    // Find out how much time has passed between cache and now.
                    TimeSpan timeSinceCache = currentLocalTime - CachedLocalTime;
                    // Use the difference in time against the cached online time to get the current time (as it would be if fetched from online).
                    // NOTE: If users change their local time or jump between time zones/date lines or if wintertime/summertime switches, this might result in errors.

                    // TODO: Perhaps make a setting that if true, will check if current datetime is in the past compared to the cached local time. If so, throw an exception that can be caught and the cache can be force synced at that point.
                    return CachedOnlineTime + timeSinceCache;
                }
            }

            public CachedDateTime() { }

            public CachedDateTime(DateTime onlineTime)
            {
                CachedOnlineTime = onlineTime;
                CachedLocalTime = DateTime.UtcNow;
            }
        }
    }
}
