﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineTimeUtil
{
    internal static class Fetcher
    {

        public static DateTime FetchTime()
        {
            FetchMethod method = OnlineTimeSettings.FetchMethod;
            
            int maxAttempts = OnlineTimeSettings.MaxFetchAttempts;
            int currentAttempts = 0;

            try
            {
                while (currentAttempts < maxAttempts)
                {
                    currentAttempts++;

                    switch (method)
                    {
                        case FetchMethod.Ntp:
                            return NtpFetcher.FetchTime();
                        case FetchMethod.Http:
                            return HttpFetcher.FetchTime();
                        default:
                            throw new ArgumentOutOfRangeException($"No valid fetch method was set.");
                    }
                }
            }
            // If we lost connection, try again until max attempts have been reached.
            catch (NoConnectionException e)
            {
                if (currentAttempts >= maxAttempts)
                {
                    throw;
                }
            }

            throw new NoConnectionException($"Max attempts reached ({maxAttempts}) while trying to fetch time from the time server.");
        }

    }
}
